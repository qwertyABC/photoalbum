package controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.Picture;
import model.User;

@WebServlet("/album")
public class AlbumController extends HttpServlet {
		/**
	 * 
	 */
	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public AlbumController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			HttpSession session = request.getSession();
			
			try {
				long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
				List<Album> albumList = dao.getUserAlbums(userId);
				List<Picture> pictureList = new ArrayList<Picture>();
				Album album = null;
				
				long albumId = getAlbumId(request);
				// if album parameter get album  and pictures from this album
				if(albumId>0){
					pictureList = dao.getUserPictures(userId, albumId);
					album = dao.getUserAlbum(userId, albumId);
				}else{
					// or all pictures
					pictureList = dao.getUserPictures(userId);
				}
				
				request.setAttribute("albumList", albumList);
				request.setAttribute("album", album);
				request.setAttribute("pictureList", pictureList);
				

			} catch (Exception e) {
				e.printStackTrace();
			}

			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "album.jsp");  
			view.forward(request,response);
		}

		private long getAlbumId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}


	
}
