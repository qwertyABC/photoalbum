package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.User;

@WebServlet("/album-new")
public class AlbumCreateController extends HttpServlet {
		/**
	 * 
	 */
	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public AlbumCreateController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "album-new.jsp");  
			view.forward(request,response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			String name = request.getParameter("name");
			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			try {
				long albumId = dao.createAlbum(userId, name);
				
				response.sendRedirect("/photoalbum/album?id="+albumId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
					
			
			
		}

	
}
