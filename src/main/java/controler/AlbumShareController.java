package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.User;

@WebServlet("/album-share")
public class AlbumShareController extends HttpServlet {
		/**
	 * 
	 */
	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public AlbumShareController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			HttpSession session = request.getSession();
			
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long albumId = getAlbumId(request);
			
			try {
				Album album = dao.getUserAlbum(userId, albumId);
				request.setAttribute("album", album);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "album-share.jsp");  
			view.forward(request,response);
		}

		private long getAlbumId(HttpServletRequest request) {
			try{
				return Long.parseLong(request.getParameter("id"));
			}catch(Exception e){
				
			}
			return 0;
		}
}