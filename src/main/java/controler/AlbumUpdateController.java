package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.User;

@WebServlet("/album-edit")
public class AlbumUpdateController extends HttpServlet {
		/**
	 * 
	 */
	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public AlbumUpdateController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			
			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long albumId = getAlbumId(request);
			
			try {
				Album album = dao.getUserAlbum(userId, albumId);
				request.setAttribute("album", album);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "album-edit.jsp");  
			view.forward(request,response);
		}

		private long getAlbumId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			String name = request.getParameter("name");
			
			
			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long albumId = getAlbumId(request);
			try {
				dao.updateAlbum(userId, getAlbumId(request), name);
				response.sendRedirect("/photoalbum/album-edit?id="+albumId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
					
			
			
		}

	
}
