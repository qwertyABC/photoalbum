package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;

import dao.Dao;
import model.User;

@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String JSP_PREFIX = "WEB-INF/pages/";

	public LoginController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "login.jsp");  
		view.forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Dao dto = (Dao)request.getServletContext().getAttribute(Dao.KEY);
		HttpSession session = request.getSession();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String hashPassword = DigestUtils.md5Hex(password);
		try{
			User user = dto.authenticateUser(email, hashPassword);
			session.setAttribute(User.SESSION_NAME, user);
			response.sendRedirect("/photoalbum/album");
			
		} catch(Exception e) {
			request.setAttribute("ERROR", e.getMessage());
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "login.jsp");  
			view.forward(request,response);
			
		} finally {

		}
	}

}
