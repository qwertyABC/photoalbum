package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.Picture;
import model.User;

@WebServlet("/picture")
public class PictureController	extends HttpServlet {

	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public PictureController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {

			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long pictureId = getPictureId(request);
			
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			try {
				Picture picture = dao.getUserPicture(userId, pictureId);
				
				long albumId = picture.getAlbumId();
				Album album = null;
				if(albumId>0){
					album = dao.getUserAlbum(userId, albumId);
					request.setAttribute("album", album);
				}
				
				request.setAttribute("picture", picture);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "picture.jsp");  
			view.forward(request,response);
			
		}
		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		}

		private long getPictureId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}

}
