package controler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;

import dao.Dao;
import model.Picture;
import model.User;
import service.FileService;

@WebServlet("/picture-view")
public class PictureViewerController	extends HttpServlet {

	private static final long serialVersionUID = 8357424123359875182L;

		public PictureViewerController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {

			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long pictureId = getPictureId(request);
			
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			FileService fileService = (FileService)request.getServletContext().getAttribute(FileService.KEY);
			try {
				Picture picture = dao.getUserPicture(userId, pictureId);
				String filePath = fileService.getFileUploadPath() + File.separator + picture.getName();;
				putToResponseStream(new File(filePath), picture.getContentType(), response);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		

		private long getPictureId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}
	    private void putToResponseStream(File picture, String contentType, HttpServletResponse response) throws IOException{
	        
	        int contentLength = (int)picture.length();
	        InputStream is = new FileInputStream(picture);
	        response.setContentLength(contentLength);
	        response.setContentType(contentType);
	        IOUtils.copy(is, response.getOutputStream());
	        response.flushBuffer();     
	    }
}
