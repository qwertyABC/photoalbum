package controler;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.Picture;
import model.User;

@WebServlet("/share")
public class SharedAlbumController extends HttpServlet {
		/**
	 * 
	 */
	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public SharedAlbumController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			HttpSession session = request.getSession();
			
			String token = request.getParameter("token");
			
			try {
				Album album = dao.getUserSharedAlbum(token);
				List<Picture> pictureList = dao.getSharedPictures(token);
				request.setAttribute("album", album);
				request.setAttribute("pictureList", pictureList);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "shared-album.jsp");  
			view.forward(request,response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			String name = request.getParameter("name");
			
			
			HttpSession session = request.getSession();
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			long albumId = getAlbumId(request);
			
			try {
				dao.updateAlbum(userId, getAlbumId(request), name);
				response.sendRedirect("/photoalbum/album-edit?id="+albumId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
					
			
			
		}

		private long getAlbumId(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return 0;
		}

	
}
