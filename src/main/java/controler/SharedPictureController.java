package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import model.Album;
import model.Picture;
import model.User;

@WebServlet("/shared-picture")
public class SharedPictureController	extends HttpServlet {

	private static final long serialVersionUID = 8357424123359875182L;
		private String JSP_PREFIX = "WEB-INF/pages/";

		public SharedPictureController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {

			long pictureId = getPictureId(request);
			String token = request.getParameter("token");
			
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			try {
			    Picture picture = dao.getSharedPicture(pictureId, token);
                request.setAttribute("picture", picture);
				Album album = dao.getUserSharedAlbum(token);
				request.setAttribute("album", album);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "shared-picture.jsp");  
			view.forward(request,response);
			
		}
		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		}

		private long getPictureId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}

}
