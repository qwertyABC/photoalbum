package controler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import dao.Dao;
import model.Picture;
import service.FileService;

@WebServlet("/shared-picture-view")
public class SharedPictureViewerController	extends HttpServlet {

	private static final long serialVersionUID = 8357424123359875182L;

		public SharedPictureViewerController() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {

			long pictureId = getPictureId(request);
			String token = request.getParameter("token");
			
			Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
			FileService fileService = (FileService)request.getServletContext().getAttribute(FileService.KEY);
			try {
				Picture picture = dao.getSharedPicture(pictureId, token);
				String filePath = fileService.getFileUploadPath() + File.separator + picture.getName();;
				putToResponseStream(new File(filePath), picture.getContentType(), response);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		

		private long getPictureId(HttpServletRequest request) {
			String aid =  request.getParameter("id");
			try{
				return Long.parseLong(aid);
			}catch(Exception e){
				
			}
			return 0;
		}
	    private void putToResponseStream(File picture, String contentType, HttpServletResponse response) throws IOException{
	        
	        int contentLength = (int)picture.length();
	        InputStream is = new FileInputStream(picture);
	        response.setContentLength(contentLength);
	        response.setContentType(contentType);
	        IOUtils.copy(is, response.getOutputStream());
	        response.flushBuffer();     
	    }
}
