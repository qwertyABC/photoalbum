package controler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dao.Dao;
import model.Album;
import model.Picture;
import model.User;
import service.FileService;

@WebServlet("/upload-picture")
public class UploadPictureController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String JSP_PREFIX = "WEB-INF/pages/";

	public UploadPictureController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Dao dao = (Dao)request.getServletContext().getAttribute(Dao.KEY);
		HttpSession session = request.getSession();
		
		try {
			long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();
			List<Album> albumList = dao.getUserAlbums(userId);
			request.setAttribute("albumList", albumList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "upload-picture.jsp");
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		if(isMultipart){
			HttpSession session =  request.getSession();
			FileService fileService = (FileService)request.getServletContext().getAttribute(FileService.KEY);
			try {
				long userId = ((User)session.getAttribute(User.SESSION_NAME)).getId();

				long pictureId= fileService.save(request, userId);
				response.sendRedirect("/photoalbum/picture?id=" + pictureId);

			} catch (Exception e) {
				request.setAttribute("ERROR", e.getMessage());
				RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "upload-picture.jsp");
				view.forward(request, response);
			}
			
		}else{
			request.setAttribute("ERROR", "No file upload!");
			
			// error
			RequestDispatcher view = request.getRequestDispatcher(JSP_PREFIX + "upload-picture.jsp");
			view.forward(request, response);
		}

		// success
	}

}
