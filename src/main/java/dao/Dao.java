package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;

import model.Album;
import model.Picture;
import model.User;

public class Dao {
	public final static String KEY = "dao";

	private DataSource dataSource;

	public Dao(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public User authenticateUser(String email, String password) throws Exception {

		String sql = "SELECT id, email, password FROM users WHERE email = ? AND password = ? ";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			rset = pstmt.executeQuery();
			if (rset.next()) {
				User user = new User();
				user.setId(rset.getLong("id"));
				user.setLogin(rset.getString("email"));
				user.setPassword(rset.getString("password"));
				return user;
			}
			throw new Exception("Incorrect email or passsword, try again...");

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}

	}

	public List<Album> getUserAlbums(Long userId) throws Exception {

		String sql = "SELECT id, name, owner_id FROM albums WHERE owner_id = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<Album> albumList = new ArrayList<Album>();
		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Album album = new Album();
				album.setId(rset.getLong("id"));
				album.setName(rset.getString("name"));
				album.setOwnerId(rset.getLong("owner_id"));
				albumList.add(album);
			}
			return albumList;

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}

	}

	public List<Picture> getUserPictures(long userId, long albumId) throws Exception {
		String sql = "SELECT id, name, contenttype, owner_id, album_id FROM pictures WHERE owner_id = ? AND album_id = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<Picture> pictureList = new ArrayList<Picture>();
		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, albumId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Picture picture = new Picture();
				picture.setId(rset.getLong("id"));
				picture.setName(rset.getString("name"));
				picture.setOwnerId(rset.getLong("owner_id"));
				picture.setAlbumId(rset.getLong("album_id"));
				pictureList.add(picture);
			}
			return pictureList;

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}

	}

	public Album getUserAlbum(long userId, long albumId) throws Exception {
		String sql = "SELECT id, name, owner_id, token FROM albums WHERE owner_id = ? AND id = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, albumId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Album album = new Album();
				album.setId(rset.getLong("id"));
				album.setName(rset.getString("name"));
				album.setOwnerId(rset.getLong("owner_id"));
				album.setToken(rset.getString("token"));
				return album;
			}
			return null;

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}
	public Album getUserSharedAlbum(String token) throws Exception {
		String sql = "SELECT id, name, owner_id, token FROM albums WHERE token = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, token);
						
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Album album = new Album();
				album.setId(rset.getLong("id"));
				album.setName(rset.getString("name"));
				album.setOwnerId(rset.getLong("owner_id"));
				album.setToken(rset.getString("token"));
				return album;
			}
			return null;

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}
	public List<Picture> getUserPictures(long userId) throws Exception {
		String sql = "SELECT id, name, contenttype, owner_id, album_id FROM pictures WHERE owner_id = ? ";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<Picture> pictureList = new ArrayList<Picture>();
		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				Picture picture = new Picture();
				picture.setId(rset.getLong("id"));
				picture.setName(rset.getString("name"));
				picture.setOwnerId(rset.getLong("owner_id"));
				picture.setAlbumId(rset.getLong("album_id"));
				pictureList.add(picture);
			}
			return pictureList;

		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

	private long getSeq() throws Exception {
		String sql = "select nextval ('hibernate_sequence');";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {

			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			rset = stmt.executeQuery(sql);
			if (rset.next()) {
				long seqId = rset.getLong(1);
				return seqId;
			}
			throw new Exception("Error retrievieng sequence number");
		} finally {
			try {
				rset.close();
			} catch (Exception e) {
			}
			try {
				stmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}

	}

	public long savePicture(long userId, String fileName, String contentType, long size, long albumId) throws Exception {
		String sql = "insert into pictures (id, name, contenttype, owner_id, size, album_id) values (?,?, ?, ?, ?, ?)";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {

			long pictureId = getSeq();

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, pictureId);
			pstmt.setString(2, fileName);
			pstmt.setString(3, contentType);
			pstmt.setLong(4, userId);
			pstmt.setLong(5, size);
			pstmt.setLong(6, albumId);
			pstmt.executeUpdate();

			return pictureId;

		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

    public Picture getUserPicture(long userId, long pictureId) throws SQLException {
        String sql = "SELECT id, name, contenttype, owner_id, album_id FROM pictures WHERE owner_id = ? AND id = ? ";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {

            conn = dataSource.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, pictureId);
            rset = pstmt.executeQuery();
            if (rset.next()) {
                Picture picture = new Picture();
                picture.setId(rset.getLong("id"));
                picture.setName(rset.getString("name"));
                picture.setOwnerId(rset.getLong("owner_id"));
                picture.setAlbumId(rset.getLong("album_id"));
                return picture;
            } else {
                throw new SQLException("Picture not found");
            }

        } finally {
            try {
                rset.close();
            } catch (Exception e) {
            }
            try {
                pstmt.close();
            } catch (Exception e) {
            }
            try {
                conn.close();
            } catch (Exception e) {
            }
        }

    }
    public Picture getSharedPicture(long pictureId, String token) throws SQLException {
        String sql = "SELECT p.id, p.name, p.contenttype, p.owner_id, p.album_id FROM pictures p, albums a WHERE p.album_id = a.id AND p.id = ?  AND a.token = ?";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {

            conn = dataSource.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, pictureId);
            pstmt.setString(2, token);
            rset = pstmt.executeQuery();
            if (rset.next()) {
                Picture picture = new Picture();
                picture.setId(rset.getLong("id"));
                picture.setName(rset.getString("name"));
                picture.setOwnerId(rset.getLong("owner_id"));
                picture.setAlbumId(rset.getLong("album_id"));
                return picture;
            } else {
                throw new SQLException("Picture not found");
            }

        } finally {
            try {
                rset.close();
            } catch (Exception e) {
            }
            try {
                pstmt.close();
            } catch (Exception e) {
            }
            try {
                conn.close();
            } catch (Exception e) {
            }
        }

    }

	public long createAlbum(long ownerId, String name) throws Exception {
		String sql = "insert into albums (id, name, owner_id, token) values (?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {

			long albumId = getSeq();
			String token = DigestUtils.md5Hex(System.currentTimeMillis() + "");
			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, albumId);
			pstmt.setString(2, name);
			pstmt.setLong(3, ownerId);
			pstmt.setString(4, token);

			pstmt.executeUpdate();

			return albumId;

		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void updateAlbum(long ownerId, long albumId, String name) throws Exception {
		String sql = "update albums set name = ? where id =? and owner_id=? ";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {

			conn = dataSource.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setLong(2, albumId);
			pstmt.setLong(3, ownerId);

			pstmt.executeUpdate();

		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
			}
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}


    public List<Picture> getSharedPictures(String token) throws Exception{
        String sql = "SELECT p.id, p.name, p.contenttype, p.owner_id, p.album_id FROM pictures p, albums a WHERE a.id = p.album_id and a.token=?";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        List<Picture> pictureList = new ArrayList<Picture>();
        try {

            conn = dataSource.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, token);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                Picture picture = new Picture();
                picture.setId(rset.getLong("id"));
                picture.setName(rset.getString("name"));
                picture.setOwnerId(rset.getLong("owner_id"));
                picture.setAlbumId(rset.getLong("album_id"));
                pictureList.add(picture);
                
            } 
            return pictureList;
        } finally {
            try {
                rset.close();
            } catch (Exception e) {
            }
            try {
                pstmt.close();
            } catch (Exception e) {
            }
            try {
                conn.close();
            } catch (Exception e) {
            }
        }    }

}
