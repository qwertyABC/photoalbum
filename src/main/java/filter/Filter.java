package filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet Filter implementation class Filter
 */
@WebFilter("/*")
public class Filter implements javax.servlet.Filter {

    /**
     * Default constructor. 
     */
    public Filter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

	    HttpServletRequest req = (HttpServletRequest)request;
	    String requestUri = req.getRequestURI();

	    if(!isLoggedIn(req.getSession())){
	        if(!isPublic(requestUri)){
	            HttpServletResponse resp = (HttpServletResponse)response;
	            resp.sendRedirect("/photoalbum/login");
	        }else{
	            chain.doFilter(request, response);
	        }
	    }else{
	        chain.doFilter(request, response);
	    }

	}

	private boolean isPublic(String requestUri) {
        if(requestUri.endsWith("/login")){
            return true;
        }
        if(requestUri.endsWith("/logout")){
            return true;
        }
        if(requestUri.endsWith("/share")){
            return true;
        }
        if(requestUri.endsWith("/shared-picture-view")){
            return true;
        }
        if(requestUri.endsWith("/shared-picture")){
            return true;
        }
        return false;
    }

    private boolean isLoggedIn(HttpSession session) {
	    User user = (User)session.getAttribute(User.SESSION_NAME);
	    if(user==null){
	        return false;
	    }
	    return true;
    }

    /**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
