package listener;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

import dao.Dao;
import service.FileService;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

	private ServletContext context;

	private DataSource dataSource = null;
	
	//Yoou must create folders on these paths
	public final static String fileUploadPath = "C:\\temp\\photoalbum\\";
	public final static String repositoryDir = "C:\\temp\\photoalbum-repo\\";
	public final static long maxFileSize = 5*1024*1024;
	public final static int maxMemSize = 5*1024*1024;

	/**
	 * Default constructor.
	 */
	public ContextListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
	}
	
	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {

		System.out.println("Application photoalbum started");
		context = sce.getServletContext();
		String dsName = "java:/comp/env/jdbc/photoalbum";
		try {
			dataSource = (DataSource) new InitialContext().lookup(dsName);
			Connection conn = dataSource.getConnection();
			conn.close();
			System.out.println("Datasource created");
			Dao dao = new Dao(dataSource);
			System.out.println("Dao created");
			context.setAttribute(Dao.KEY, dao);
			
			
			FileService fileService = new FileService();
			fileService.setDao(dao);
			fileService.setFileUploadPath(fileUploadPath);
			fileService.setMaxFileSize(maxFileSize);
			fileService.setMaxMemSize(maxMemSize);
			fileService.setRepositoryDir(repositoryDir);
			context.setAttribute(FileService.KEY, fileService);
			
		} catch (NamingException | SQLException e) {
			e.printStackTrace();
		}

	}

}
