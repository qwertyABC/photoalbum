package service;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dao.Dao;
import model.Picture;

public class FileService {

	public static final String KEY = "FileService";
	private int maxMemSize;
	private long maxFileSize;
	private String repositoryDir;
	private String fileUploadPath;

	private Dao dao;

	public FileService() {

	}

	public long save(HttpServletRequest request, long userId) throws Exception {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File(repositoryDir));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		// Parse the request to get file items.
		List<FileItem> fileItems = upload.parseRequest(request);

		// Process the uploaded file items
		Iterator<FileItem> i = fileItems.iterator();
		File file = null;
		long albumId = 0;
		Picture picture = null;
		while (i.hasNext()) {
			FileItem fi = (FileItem) i.next();
			
			if (!fi.isFormField()) {
				// Get the uploaded file parameters
				String fieldName = fi.getFieldName();
				String fileName = fi.getName();
				String contentType = fi.getContentType();
				boolean isInMemory = fi.isInMemory();
				long sizeInBytes = fi.getSize();
				// Write the file
				if (fileName.lastIndexOf("\\") >= 0) {
					file = new File(fileUploadPath + fileName.substring(fileName.lastIndexOf("\\")));
				} else {
					file = new File(fileUploadPath + fileName.substring(fileName.lastIndexOf("\\") + 1));
				}
				fi.write(file);

				picture = new Picture();
				picture.setSize(sizeInBytes);
				picture.setName(fileName);
				picture.setContentType(contentType);
				
			}else{
			    albumId = getAlbumId(fi);
			}
		}
		if(picture!=null){
		    return dao.savePicture(userId, picture.getName(), picture.getContentType(), picture.getSize(), albumId);
		}
		return 0;
	}

    private long getAlbumId(FileItem fi) {
        if("albumId".equals(fi.getFieldName())){
            try{
                long albumId = Long.parseLong(fi.getString());
                return albumId;
            }catch(Exception e){}
        }
        return 0;
    }

    public int getMaxMemSize() {
		return maxMemSize;
	}

	public void setMaxMemSize(int maxMemSize) {
		this.maxMemSize = maxMemSize;
	}

	public long getMaxFileSize() {
		return maxFileSize;
	}

	public void setMaxFileSize(long maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	public String getRepositoryDir() {
		return repositoryDir;
	}

	public void setRepositoryDir(String repositoryDir) {
		this.repositoryDir = repositoryDir;
	}

	public String getFileUploadPath() {
		return fileUploadPath;
	}

	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}

	public Dao getDao() {
		return dao;
	}

	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
