﻿-- Role: photoalbum

-- DROP ROLE photoalbum;

CREATE ROLE photoalbum LOGIN
  PASSWORD 'photoalbum'
  NOSUPERUSER INHERIT CREATEDB CREATEROLE NOREPLICATION VALID UNTIL '2025-12-01 00:00:00';

-- Database: photoalbum

--DROP DATABASE photoalbum;

CREATE DATABASE photoalbum
  WITH OWNER = photoalbum
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Polish_Poland.1250'
       LC_CTYPE = 'Polish_Poland.1250'
       CONNECTION LIMIT = -1;

-- Tabels: users
-- DROP TABLE users;
CREATE TABLE users
(
  id bigint NOT NULL,
  email character varying(255) NOT NULL,
  password character varying(40) NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO photoalbum;

--DROP TABLE albums;
CREATE TABLE albums
(
  id bigint NOT NULL,
  name character varying(255),
  token character varying(255),
  owner_id bigint NOT NULL,
  CONSTRAINT album_pkey PRIMARY KEY (id),
  CONSTRAINT fk_users_album FOREIGN KEY (owner_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE albums
  OWNER TO photoalbum;
  

--DROP TABLE pictures;
CREATE TABLE pictures
(
  id bigint NOT NULL,
  name character varying(255),
  contenttype character varying(255) NOT NULL,
  size bigint,
  owner_id bigint NOT NULL,
  album_id bigint,
  CONSTRAINT picture_pkey PRIMARY KEY (id),
  CONSTRAINT fk_users_pictures FOREIGN KEY (owner_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_album_picture FOREIGN KEY (album_id)
      REFERENCES albums (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pictures
  OWNER TO photoalbum;
  




-- Sequence: hibernate_sequence

--DROP SEQUENCE hibernate_sequence;

CREATE SEQUENCE hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 200
  CACHE 1;
ALTER TABLE hibernate_sequence
  OWNER TO photoalbum;
