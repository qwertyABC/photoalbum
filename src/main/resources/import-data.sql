insert into users (id,  email, password) values (1, 'tynka013@gmail.com','c4ca4238a0b923820dcc509a6f75849b');	--pass:1
insert into albums (id,  name, owner_id) values (10, 'Majorka ',1);
insert into albums (id,  name, owner_id) values (11, 'Narty ',1);

insert into pictures (id, name, contenttype,  owner_id, album_id) values (20,'DSCN001.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (21,'DSCN002.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (22,'DSCN003.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (23,'DSCN004.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (24,'DSCN005.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (25,'DSCN006.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (26,'DSCN007.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (27,'DSCN008.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (28,'DSCN009.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (29,'DSCN010.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (30,'DSCN011.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (31,'DSCN012.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (32,'DSCN013.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (33,'DSCN014.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (34,'DSCN015.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (35,'DSCN016.JPG', 'image/jpg', 1, 11);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (36,'DSCN017.JPG', 'image/jpg', 1, 10);
insert into pictures (id, name, contenttype,  owner_id, album_id) values (37,'DSCN018.JPG', 'image/jpg', 1, 11);

select nextval ('hibernate_sequence');
