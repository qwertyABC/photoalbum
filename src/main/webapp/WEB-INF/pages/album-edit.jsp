<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jspf"%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>


<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-10">
		<h3>Edit the name of the album</h3>
		<form action="/photoalbum/album-edit" method="post">
			<input type="hidden" name="id" value="${album.id}" />
			<input	type="text" name="name" value="${album.name}" /> 
			<input	type="submit" value="Edytuj" />
		</form>
			<a href="/photoalbum/album"> Go back</a>
	</div>
</div>





<%@include file="footer.jspf"%>