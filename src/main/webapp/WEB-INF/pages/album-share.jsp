<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jspf"%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>


<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-10">
		<h3>Share album: ${album.name} </h3>
		<h4>Copy link below to share it with friend.</h4>
		
			<input type="text" name="urlAlbum" style="width:550px;" value="http://localhost:8080/photoalbum/share?token=${album.token}" /> 
			
			<a href="/photoalbum/album?id=${ album.id}">
			<br>Go back</a>
		
	</div>
</div>





<%@include file="footer.jspf"%>