<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jspf"%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>


<div class="row">
	<div class="col-md-2">
		<h3>Album list:</h3>
		<div class="list-group">
			<a href="/photoalbum/album" class="list-group-item">All pictures</a>

			<c:forEach items="${albumList}" var="album">

				<a href="/photoalbum/album?id=${album.id}" class="list-group-item">${album.name}</a>
			</c:forEach>
		</div>
		
		<a class="btn btn-primary" href="/photoalbum/album-new">Add album</a><br><br>
		<a class="btn btn-primary" href="/photoalbum/upload-picture">Add picture</a>

		<!-- <form action="/photoalbum/upload-picture" method="POST"
			enctype="multipart/form-data">
			<br>
			<input type="file" name="file"><br>
			 <input type="submit"
				class="btn btn-primary" value="Upload File" />
				
		</form> -->
	</div>
	<div class="col-md-10">
		<c:if test="${album eq null }">
		
			<h3>All your pictures</h3>
		</c:if>
		
		<c:if test="${album ne null }">
				
			<h3>Album: <strong>${album.name}</strong></h3>
			
				<h5><a href="/photoalbum/album-edit?id=${album.id}">Edit name album</a>
				&nbsp &nbsp
				<a href="/photoalbum/album-share?id=${album.id}">Share album</a>
			</h5>
		</c:if>
		
		<c:forEach items="${pictureList}" var="picture">
			<a href="/photoalbum/picture?id=${picture.id}" class="thumbnail"
				style="float: left; display: block; margin-right: 5px;"> <img
				data-src="holder.js/100%x180" alt="100%x180"
				src="/photoalbum/picture-view?id=${picture.id}"
				data-holder-rendered="true"
				style="height: 180px; width: 220px; display: block;">
			</a>

		</c:forEach>

	</div>
	
</div>





<%@include file="footer.jspf"%>