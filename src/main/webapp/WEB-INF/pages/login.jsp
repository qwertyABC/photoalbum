<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="header.jspf"%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">
			Best Photoalbum for collecting and sharing memories
		</p>
	</div>
	<div class="col-md-3"></div>

</div>


<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<form class="form-signin" method="POST">
			<c:if test="${ERROR ne null}">
				<p class="bg-danger">${ERROR}</p>
			</c:if>
		
			<h2 class="form-signin-heading">Please sign in</h2>
			<label for="inputEmail" class="sr-only">Email address</label> <input
				type="email" name="email" id="inputEmail" class="form-control"
				placeholder="Email address" required autofocus><br> <label
				for="inputPassword" class="sr-only">Password</label> <input
				type="password" name="password" id="inputPassword" class="form-control"
				placeholder="Password" required><br>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
				in</button>
		</form>


	</div>
	<div class="col-md-4"></div>
</div>





<%@include file="footer.jspf"%>