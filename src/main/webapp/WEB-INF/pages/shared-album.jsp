<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>

<div class="row">
	
	<div class="col-md-10">

			<h3>Album: <strong>${album.name}</strong></h3>
										
		<c:forEach items="${pictureList}" var="picture">
			<a href="/photoalbum/shared-picture?id=${picture.id}&token=${album.token}" class="thumbnail"
				style="float: left; display: block; margin-right: 5px;"> <img
				data-src="holder.js/100%x180" alt="100%x180"
				src="/photoalbum/shared-picture-view?id=${picture.id}&token=${album.token}"
				data-holder-rendered="true"
				style="height: 180px; width: 220px; display: block;">
			</a>

		</c:forEach>

	</div>
	
</div>

<%@include file="footer.jspf"%>