<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- <%@include file="header.jspf"%> --%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>


<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-10">
		<c:if test="${album ne null }">
			<h3>Album: ${album.name}</h3>
		</c:if>

		Picture name: <strong> ${picture.name}</strong>
		<br><br>
		<img src="/photoalbum/shared-picture-view?id=${picture.id}&token=${album.token}" >
		<br>
		<a href="/photoalbum/share?token=${album.token}">Go back</a>

	</div>
</div>





<%@include file="footer.jspf"%>