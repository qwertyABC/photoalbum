<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jspf"%>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h1>Photoalbum application</h1>
		<p class="lead">Best Photoalbum for collecting and sharing
			memories</p>
	</div>
	<div class="col-md-3"></div>
</div>

<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-10">
		<h3>Upload picture</h3>
		<c:if test="${ERROR ne null}">
			<p class="bg-danger">${ERROR}</p>
		</c:if>

		<form action="/photoalbum/upload-picture" method="POST" enctype="multipart/form-data">
			<input type="file"  name="file"> <br>
			Choose album:<br/>
			<select name="albumId">
			<c:forEach items="${albumList}" var="album">
			<option value="${album.id}">${album.name}</option>
			</c:forEach>
			</select> <br> <br>
			<input type="submit" class="btn btn-primary" value="Upload File" />
		</form> <br>
			<a href="/photoalbum/album"> Go back</a>
	</div>
</div>

<%@include file="footer.jspf"%>